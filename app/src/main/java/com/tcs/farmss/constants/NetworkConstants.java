package com.tcs.farmss.constants;

/**
 * Created by Harsh on 3/9/2017.
 */
public interface NetworkConstants {

    String GET_NETWORK_IP = "http://farmss1-hoagsobjtech.rhcloud.com/";
    String LOGIN_URL = GET_NETWORK_IP + "/login";
    String REGISTRATION_URL = GET_NETWORK_IP + "/register";
    String RESET_PASSWORD_URL = GET_NETWORK_IP + "/resetpass";
    String RESET_PASSWORD_CHANGE_URL = RESET_PASSWORD_URL + "/chg";
    String GET_USER_URL = GET_NETWORK_IP + "/users";
    String ADD_FARMER_URL = GET_NETWORK_IP + "/addfarmer";
    String ADD_FARMER_SYNC_URL = ADD_FARMER_URL + "/bulk";
    String GET_FARMER_URL = GET_NETWORK_IP + "/farmers";
}

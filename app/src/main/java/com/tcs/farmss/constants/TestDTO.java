package com.tcs.farmss.constants;

/**
 * Created by Harsh on 1/20/2017.
 */
public class TestDTO {
    private String name;
    private int thumbnail;

    public TestDTO(){
    }

    public TestDTO(String name, int thumbnail){
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}

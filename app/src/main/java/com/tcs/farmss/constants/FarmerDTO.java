package com.tcs.farmss.constants;

/**
 * Created by Harsh on 3/14/2017.
 */
public class FarmerDTO {
    public String name;
    public String mobile;
    public String state;
    public String district;
    public String tehsil;
    public String village;
    public String pincode;

    public FarmerDTO() {

    }

    public FarmerDTO(String name, String mobile, String village, String state, String district, String tehsil, String pincode) {
        this.name = name;
        this.mobile = mobile;
        this.village = village;
        this.state = state;
        this.district = district;
        this.tehsil = tehsil;
        this.pincode = pincode;
    }
}

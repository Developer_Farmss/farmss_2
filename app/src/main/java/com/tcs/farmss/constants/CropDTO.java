package com.tcs.farmss.constants;

/**
 * Created by Harsh on 1/25/2017.
 */
public class CropDTO {
    private String cropName;
    private int cropIcon;

    public CropDTO() {
    }

    public CropDTO(String cropName, int cropIcon){
        this.cropName = cropName;
        this.cropIcon = cropIcon;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public int getCropIcon() {
        return cropIcon;
    }

    public void setCropIcon(int cropIcon) {
        this.cropIcon = cropIcon;
    }
}

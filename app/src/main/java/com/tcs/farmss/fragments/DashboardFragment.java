package com.tcs.farmss.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.tcs.farmss.R;
import com.tcs.farmss.activities.AddFarmerActivity;
import com.tcs.farmss.activities.DashboardActivity;
import com.tcs.farmss.activities.FarmerProfileActivity;
import com.tcs.farmss.adapters.DashboardAdapter;
import com.tcs.farmss.constants.FarmerDTO;
import com.tcs.farmss.constants.NetworkConstants;
import com.tcs.farmss.database.DBActivity;
import com.tcs.farmss.global.MainApplication;
import com.tcs.farmss.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    private View parentView;
    private DashboardAdapter adapter;
    @Bind(R.id.recyclerBlank)
    RecyclerView recyclerBlank;
    DBActivity db;
    @Bind(R.id.fabAddFarmer)
    FloatingActionButton fabAddFarmer;
    List<FarmerDTO> farmerDTOList;
    private List<FarmerDTO> items;
    private MaterialDialog dialog;
    private static final String TAG = DashboardActivity.class.getSimpleName();


    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, parentView);
        setHasOptionsMenu(true);
        populate();
        return parentView;
    }

    private void populate() {

        fabAddFarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddFarmerActivity.class));
            }
        });

        db = new DBActivity(getActivity());
        db.open();
        farmerDTOList = db.getFarmerData();
        db.close();
        if (farmerDTOList == null) {
            Snackbar.show(getActivity(), getString(R.string.no_data_present));
        }

        recyclerBlank.setAdapter(new DashboardAdapter(getActivity(), farmerDTOList, new DashboardAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FarmerDTO item) {
                Bundle bundle = new Bundle();
                bundle.putString("name", item.name);
                bundle.putString("mobile", item.mobile);
                bundle.putString("state", item.state);
                bundle.putString("district", item.district);
                bundle.putString("tehsil", item.tehsil);
                bundle.putString("village", item.village);
                bundle.putString("pincode", item.pincode);
                Intent intent = new Intent(getActivity(), FarmerProfileActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }));

        recyclerBlank.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh) {
            ArrayList<HashMap<String, String>> farmerList = db.getAllFarmers();
            if (farmerList.size() != 0) {
                int t = db.dbSyncCount();
                if (t != 0) {
                    syncSQLite();
                } else {
                    Snackbar.show(getActivity(), getString(R.string.data_already_synced));
                }
            } else {
                Snackbar.show(getActivity(), getString(R.string.no_data_to_sync));
            }
        }
        return true;
    }

    private void syncSQLite() {

        String tag_str_req = "req_sync";

        dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.sync)
                .content(R.string.sync_message)
                .progress(true, 0)
                .show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("farmers", db.composeJSONFromSQLite());
            Log.i("params:::", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(NetworkConstants.ADD_FARMER_SYNC_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.i(TAG, response.toString());
                try {
                    boolean res = response.getBoolean("response");
                    if (res) {
                        db.updateSyncStatus("yes");
                        Snackbar.success(getActivity(), getString(R.string.sync_success));
                    } else {
                        Log.e(TAG, "Error");
                        Snackbar.show(getActivity(), getString(R.string.error_message));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG, "Volley Error: " + error.getMessage());
                Snackbar.show(getActivity(), getString(R.string.network_error));
            }
        });
/*
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.ADD_FARMER_SYNC_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean res = jsonObject.getBoolean("response");

                    if (res) {
                        db.updateSyncStatus("yes");
                        Snackbar.show(getActivity(), "Data Successfully Synced...");
                    } else {
                        Log.e(TAG, "Error");
                        Snackbar.show(getActivity(), "Some error occurred!!!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.show(getActivity(), "Some error occurred!!!");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG, "Volley Error: " + error.getMessage());
                Snackbar.show(getActivity(), "Network Error");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {
                    //JSONObject jsonObject = new JSONObject();
                    String farmers = db.composeJSONFromSQLite().toString();
                    Log.i("farmers::",farmers);
                    //jsonObject.put("farmers", farmers);
                    //String a = jsonObject.toString();
                    params.put("", farmers);

                    Log.i("params:::", params.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return params;
            }
        };*/
        MainApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_str_req);
    }
}


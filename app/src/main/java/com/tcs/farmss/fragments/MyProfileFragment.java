package com.tcs.farmss.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmss.R;
import com.tcs.farmss.activities.DashboardActivity;
import com.tcs.farmss.activities.EditProfileActivity;
import com.tcs.farmss.adapters.ProfileAdapter;
import com.tcs.farmss.constants.NetworkConstants;
import com.tcs.farmss.constants.ProfileInfo;
import com.tcs.farmss.global.MainApplication;
import com.tcs.farmss.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {

    private View parentView;
    private ProfileAdapter adapter;
    @Bind(R.id.profileRecyclerView)
    RecyclerView profileRecyclerView;
    private MaterialDialog dialog;
    private SharedPreferences preferences;
    private static final String TAG = DashboardActivity.class.getSimpleName();
    String userName;
    String userMobile;
    String userEmail;
    String userLocation;
    private List<ProfileInfo> data;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, parentView);
        setHasOptionsMenu(true);
        preferences = getActivity().getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
        return parentView;
    }

    private void populate() {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.please_wait)
                .content(R.string.fetching)
                .progress(true, 0)
                .show();


        String userId = preferences.getString("userId", null);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NetworkConstants.GET_USER_URL + "/" + userId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Profile Response: " + response.toString());
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    userName = jsonObject.getString("name");
                    userMobile = jsonObject.getString("mobile");
                    userEmail = jsonObject.getString("email");
                    userLocation = jsonObject.getString("location");
                    data = new ArrayList<>();
                    String[] attributes = {"Name", "Phone No.", "Email", "Work Location"};
                    String[] values = {userName, userMobile, userEmail, userLocation};

                    for (int i = 0; i < attributes.length && i < values.length; i++) {
                        ProfileInfo current = new ProfileInfo();
                        current.attribute = attributes[i];
                        current.value = values[i];
                        data.add(current);
                    }
                    adapter = new ProfileAdapter(getActivity(), data);
                    profileRecyclerView.setAdapter(adapter);
                    profileRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error response: " + error.getMessage());
                Snackbar.show(getActivity(), error.getMessage());
                dialog.dismiss();
            }
        });
        MainApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.profileEdit) {
            startActivity(new Intent(getActivity(), EditProfileActivity.class));
        }
        return super.onOptionsItemSelected(item);

    }
}

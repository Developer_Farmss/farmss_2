package com.tcs.farmss.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joanzapata.pdfview.PDFView;
import com.tcs.farmss.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsAndConditionsFragment extends Fragment {


    @Bind(R.id.pdfTerms)
    PDFView pdfTerms;
    private View parentView;

    public TermsAndConditionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView =  inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
        ButterKnife.bind(this, parentView);
        pdfTerms.fromAsset("terms_and_conditions.pdf").load();
        return parentView;
    }
}

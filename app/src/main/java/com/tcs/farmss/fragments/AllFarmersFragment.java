package com.tcs.farmss.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmss.R;
import com.tcs.farmss.adapters.FarmersAdapter;
import com.tcs.farmss.constants.FarmerDTO;
import com.tcs.farmss.constants.NetworkConstants;
import com.tcs.farmss.global.MainApplication;
import com.tcs.farmss.ui.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllFarmersFragment extends Fragment {


    private View parentView;
    private FarmersAdapter adapter;
    List<FarmerDTO> farmerDTOList;
    private MaterialDialog dialog;
    @Bind(R.id.listFarmers)
    RecyclerView recyclerFarmers;

    public AllFarmersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_all_farmers, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {

        dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.please_wait)
                .content(R.string.fetching)
                .progress(true, 0)
                .show();

        farmerDTOList = new ArrayList<FarmerDTO>();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, NetworkConstants.GET_FARMER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.i("Response:::", response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        /*String name = jsonObject1.getString("name");
                        String mobile = jsonObject1.getString("mobile");
                        String state = jsonObject1.getString("state");
                        String district = jsonObject1.getString("district");
                        String tehsil = jsonObject1.getString("tehsil");
                        String village = jsonObject1.getString("village");
                        String pincode = jsonObject1.getString("pincode");*/
                        farmerDTOList.add(new FarmerDTO(
                                jsonObject1.getString("name"),
                                jsonObject1.getString("mobile"),
                                jsonObject1.getString("state"),
                                jsonObject1.getString("district"),
                                jsonObject1.getString("tehsil"),
                                jsonObject1.getString("village"),
                                jsonObject1.getString("pincode")
                        ));
                        recyclerFarmers.setAdapter(new FarmersAdapter(getActivity(), farmerDTOList, new FarmersAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(FarmerDTO item) {

                            }
                        }));
                        recyclerFarmers.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error:::", error.getMessage());
                Snackbar.show(getActivity(), getString(R.string.error_message));
            }
        });
        MainApplication.getInstance().addToRequestQueue(stringRequest);


    }
}

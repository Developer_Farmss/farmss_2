package com.tcs.farmss.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcs.farmss.R;
import com.tcs.farmss.constants.FarmerDTO;
import com.tcs.farmss.constants.ProfileInfo;

import java.util.Collections;
import java.util.List;

/**
 * Created by Harsh on 3/14/2017.
 */
public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardViewHolder> {
    private LayoutInflater inflater;
    List<ProfileInfo> data = Collections.emptyList();
    List<FarmerDTO> farmerDTOList = Collections.emptyList();
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(FarmerDTO item);
    }

    //    private List<FarmerDTO> items;
    private OnItemClickListener listener;

    /*public DashboardAdapter(List<FarmerDTO> items, OnItemClickListener listener){
        this.items = items;
        this.listener = listener;
    }*/

    public DashboardAdapter(Context context, List<FarmerDTO> farmerDTOList, OnItemClickListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.farmerDTOList = farmerDTOList;
        this.listener = listener;
    }


    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout, parent, false);
        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout, parent, false);
        DashboardViewHolder holder = new DashboardViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        holder.bind(farmerDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return farmerDTOList.size();
    }

    class DashboardViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlFarmerCard;
        TextView txtName;
        TextView txtMobile;
        TextView txtVillage;

        public DashboardViewHolder(View itemView) {
            super(itemView);

            rlFarmerCard = (RelativeLayout) itemView.findViewById(R.id.rlFarmerCard);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtMobile = (TextView) itemView.findViewById(R.id.txtMobile);
            txtVillage = (TextView) itemView.findViewById(R.id.txtAddress);
        }

        public void bind(final FarmerDTO farmerDTO, final OnItemClickListener listener) {
            txtName.setText(farmerDTO.name);
            txtMobile.setText(farmerDTO.mobile);
            txtVillage.setText(farmerDTO.village);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(farmerDTO);
                }
            });
        }
    }
}

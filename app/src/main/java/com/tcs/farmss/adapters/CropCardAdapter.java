package com.tcs.farmss.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tcs.farmss.R;
import com.tcs.farmss.activities.RecommendFertilizerActivity;
import com.tcs.farmss.constants.CropDTO;

import java.util.List;

/**
 * Created by Harsh on 1/25/2017.
 */
public class CropCardAdapter extends RecyclerView.Adapter<CropCardAdapter.MyViewHolder> {

    private Context context;
    private List<CropDTO> cropList;

    public CropCardAdapter(Context context, List<CropDTO> cropList) {
        this.context = context;
        this.cropList = cropList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.crop_selection_custom_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CropDTO cropDTO = cropList.get(position);
        holder.cropName.setText(cropDTO.getCropName());
        Glide.with(context).load(cropDTO.getCropIcon()).into(holder.cropIcon);
    }


    @Override
    public int getItemCount() {
        return cropList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView cropName;
        public ImageView cropIcon;

        public MyViewHolder(View view) {
            super(view);
            cropName = (TextView) view.findViewById(R.id.txtCropName);
            cropIcon = (ImageView) view.findViewById(R.id.imgCropIcon);
            cropIcon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            context.startActivity(new Intent(context, RecommendFertilizerActivity.class));
        }
    }
}


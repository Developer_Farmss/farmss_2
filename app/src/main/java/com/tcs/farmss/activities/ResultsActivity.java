package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.button.ButtonPlus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 1/25/2017.
 */
public class ResultsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.pH_result)
    TextView phResult;
    @Bind(R.id.N_result)
    TextView nResult;
    @Bind(R.id.P_result)
    TextView pResult;
    @Bind(R.id.K_result)
    TextView kResult;
    @Bind(R.id.btnSelectCrop)
    ButtonPlus btnSelectCrop;
    SharedPreferences preferences;


    @OnClick(R.id.btnSelectCrop)
    void selectCrop(){
        startActivity(new Intent(this, CropSelectionActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.test_results)));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSelectCrop.setBackgroundResource(R.drawable.ripple);
        }

        nResult.setText(preferences.getString("nitrate", null));
    }

    @Override
    public void onBackPressed() {

    }
}

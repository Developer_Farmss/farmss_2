package com.tcs.farmss.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmss.R;
import com.tcs.farmss.constants.NetworkConstants;
import com.tcs.farmss.global.MainApplication;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 1/19/2017.
 */
public class ForgotPasswordActivity extends AppCompatActivity {

    @Bind(R.id.etForgotPassword)
    EditText etForgotPassword;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnForgotPassword)
    ButtonPlus btnForgotPassword;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private MaterialDialog dialog;


    @OnClick(R.id.btnForgotPassword)
    void forgotPassword() {
        if (etForgotPassword.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.reset_password_email_alert));
            return;
        }
        else {
            resetPassword(etForgotPassword.getText().toString());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.ForgetPassword)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnForgotPassword.setBackgroundResource(R.drawable.ripple);
        }
    }

    private void resetPassword(final String email) {

        String tag_str_req = "req_forgot_password";

        dialog = new MaterialDialog.Builder(ForgotPasswordActivity.this)
                .title(R.string.progress_dialog_forgot_password_title)
                .content(R.string.progress_dialog_forgot_password)
                .progress(true, 0)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.RESET_PASSWORD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Response: " + response.toString());
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean res = jsonObject.getBoolean("res");

                    if (res) {
                        Intent intent = new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class);
                        intent.putExtra("email", email);
                        startActivity(intent);
                    } else {
                        String error_msg = jsonObject.getString("response");
                        Snackbar.show(ForgotPasswordActivity.this, error_msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                return params;
            }
        };
        MainApplication.getInstance().addToRequestQueue(stringRequest, tag_str_req);
    }
}

package com.tcs.farmss.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.akexorcist.localizationactivity.LocalizationDelegate;
import com.tcs.farmss.R;
import com.tcs.farmss.util.SessionManager;

import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/19/2017.
 */
public class SplashActivity extends LocalizationActivity {

    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(getApplicationContext());

        Thread thread = new Thread() {
            public void run() {
                try {
                    if (sessionManager.checkLang().equals("english")){
                        setLanguage("en");
                    } else if (sessionManager.checkLang().equals("hindi")){
                        setLanguage("hi");
                    } else if (sessionManager.checkLang().equals("marathi")){
                        setLanguage("mr");
                    }
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (sessionManager.isLoggedIn()) {
                        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                }
            }
        };
        thread.start();
    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
    }
}

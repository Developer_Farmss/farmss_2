package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmss.R;
import com.tcs.farmss.constants.NetworkConstants;
import com.tcs.farmss.global.MainApplication;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;
import com.tcs.farmss.util.EmailValidator;
import com.tcs.farmss.util.NetworkCheck;
import com.tcs.farmss.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/19/2017.
 */
public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etMobileReg)
    EditText etMobileReg;
    @Bind(R.id.etWorkLocation)
    EditText etWorkLocation;
    @Bind(R.id.etPasswordReg)
    EditText etPasswordReg;
    @Bind(R.id.etConfirmPasswordReg)
    EditText etConfirmPasswordReg;
    @Bind(R.id.btnRegister)
    ButtonPlus btnRegister;
    @Bind(R.id.checkboxTerms)
    CheckBox checkboxTerms;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private MaterialDialog dialog;
    private SessionManager sessionManager;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.RegisterActivityLable)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnRegister.setBackgroundResource(R.drawable.ripple);
        }

        /*dialog = new MaterialDialog.Builder(RegisterActivity.this)
                .title(R.string.progress_dialog_register)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();*/

        sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggedIn()) {
            startActivity(new Intent(RegisterActivity.this, DashboardActivity.class));
            finish();
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(etName.getText().toString().trim().equals("") || etEmail.getText().toString().trim().equals("")
                        || etMobileReg.getText().toString().trim().equals("")
                        || etWorkLocation.getText().toString().trim().equals("")
                        || etPasswordReg.getText().toString().trim().equals("")
                        || etConfirmPasswordReg.getText().toString().trim().equals(""))) {
                    if (etMobileReg.getText().length() == 10) {
                        if (etPasswordReg.getText().toString().equals(etConfirmPasswordReg.getText().toString())) {
                            if (checkboxTerms.isChecked()) {
                                if (!EmailValidator.isValidEmail(etEmail.getText().toString())) {
                                    if (NetworkCheck.isNetworkAvailable(RegisterActivity.this)) {
                                        registerUser(etName.getText().toString(), etEmail.getText().toString(),
                                                etMobileReg.getText().toString(), etPasswordReg.getText().toString(),
                                                etWorkLocation.getText().toString());
                                    } else {
                                        Snackbar.show(RegisterActivity.this, getString(R.string.no_internet));
                                    }
                                } else {
                                    Snackbar.show(RegisterActivity.this, getString(R.string.invalid_email));
                                }
                            } else {
                                Snackbar.show(RegisterActivity.this, getString(R.string.terms_conditions_alert));
                            }
                        } else {
                            Snackbar.show(RegisterActivity.this, getString(R.string.matching_passwords));
                        }
                    } else {
                        Snackbar.show(RegisterActivity.this, getString(R.string.invalid_mobile));
                    }
                } else {
                    Snackbar.show(RegisterActivity.this, getString(R.string.no_text));
                }
            }
        });
    }

    private void registerUser(final String employeeName, final String email, final String mobile,
                              final String password, final String workLocation) {

        String tag_string_req = "req_register";

        dialog = new MaterialDialog.Builder(RegisterActivity.this)
                .title(R.string.progress_dialog_register)
                .content(R.string.progress_dialog_register)
                .progress(true, 0)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.REGISTRATION_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(RegisterActivity.class.getSimpleName(), "Register response" + response.toString());
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String res = jsonObject.getString("response");
                    if (res.equals("Successfully Registered...")) {
                        sessionManager.setLogin(true);
                        Intent intent = new Intent(RegisterActivity.this, DashboardActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("register", "register");
                        intent.putExtras(bundle);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(RegisterActivity.this);
                    } else {
                        String error_msg = jsonObject.getString("response");
                        Snackbar.show(RegisterActivity.this, error_msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(RegisterActivity.class.getSimpleName(), "Registration Error: " + error.getMessage());
                Snackbar.show(RegisterActivity.this, getString(R.string.network_error));
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("employeeName", employeeName);
                params.put("email", email);
                params.put("mobile", mobile);
                params.put("password", password);
                params.put("workLocation", workLocation);
                return params;
            }
        };

        MainApplication.getInstance().addToRequestQueue(stringRequest);
    }
}

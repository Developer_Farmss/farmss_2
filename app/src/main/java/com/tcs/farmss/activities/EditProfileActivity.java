package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmss.R;
import com.tcs.farmss.constants.NetworkConstants;
import com.tcs.farmss.global.MainApplication;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;
import com.tcs.farmss.util.EmailValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harshdeepsingh on 18/03/17.
 */

public class EditProfileActivity extends AppCompatActivity {

    @Bind(R.id.etNameEdit)
    EditText etNameEdit;
    @Bind(R.id.etMobileEdit)
    EditText etMobileEdit;
    @Bind(R.id.etEmailEdit)
    EditText etEmailEdit;
    @Bind(R.id.etWorkLocationEdit)
    EditText etWorkLocationEdit;
    @Bind(R.id.btnProfileEdit)
    ButtonPlus btnProfileEdit;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private MaterialDialog dialog;
    private SharedPreferences preferences;

    @OnClick(R.id.btnProfileEdit)
    void alertProfileChange() {
        if (etNameEdit.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_name_alert));
            return;
        }
        if (etEmailEdit.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_email_alert));
            return;
        }
        if (etMobileEdit.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_mobile_alert));
            return;
        }
        if (etMobileEdit.getText().length() != 10) {
            Snackbar.show(this, getString(R.string.invalid_mobile));
            return;
        }
        if (etWorkLocationEdit.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.work_location_alert));
            return;
        }
        if (EmailValidator.isValidEmail(etEmailEdit.getText().toString())) {
            Snackbar.show(this, getString(R.string.invalid_email));
        } else {
            editProfile(etNameEdit.getText().toString(), etMobileEdit.getText().toString(), etEmailEdit.getText().toString(),
                    etWorkLocationEdit.getText().toString());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.edit_profile)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnProfileEdit.setBackgroundResource(R.drawable.ripple_rounded);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void editProfile(final String name, final String mobile, final String email, final String location) {
        dialog = new MaterialDialog.Builder(EditProfileActivity.this)
                .title(R.string.please_wait)
                .content(R.string.edit_profile_dialog_message)
                .progress(true, 0)
                .show();

        String userId = preferences.getString("userId", null);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, NetworkConstants.GET_USER_URL + "/" + userId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    if (message.equals("User Updated...")) {
                        Intent intent = new Intent(EditProfileActivity.this, DashboardActivity.class);
                        intent.putExtra("user updated", "success");
                        startActivity(intent);
                    } else {
                        Snackbar.show(EditProfileActivity.this, getString(R.string.error_message));
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error::", error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("employeeName", name);
                params.put("email", email);
                params.put("mobile", mobile);
                params.put("workLocation", location);
                return params;
            }
        };
        MainApplication.getInstance().addToRequestQueue(stringRequest);
    }
}

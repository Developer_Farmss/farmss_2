package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harshdeepsingh on 21/03/17.
 */

public class NitrateTestActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnSelectImage)
    ButtonPlus btnSelectImage;
    @Bind(R.id.btnClickImage)
    ButtonPlus btnClickImage;
    @Bind(R.id.imgStripPicture)
    ImageView imgStripPicture;
    @Bind(R.id.txtGValue)
    TextView txtGvalue;
    @Bind(R.id.txtCorrectedGValue)
    TextView txtCorrectedGValue;
    @Bind(R.id.txtStripReading)
    TextView txtStripReading;
    @Bind(R.id.txtNitrateConcentration)
    TextView txtNitrateConcentration;
    @Bind(R.id.btnAnotherTest)
    ButtonPlus btnAnotherTest;
    SharedPreferences preferences;


    private static final String TAG = "Nitrate Test Activity";
    private Mat mat;
    private static final int CAMERA_REQUEST = 1777;
    int j = 1, deltaR, deltaG, deltaB;
    int r_ref = 116, g_ref = 130, b_ref = 125;
    double quadCoeff1 = 0.045632, linearCoeff1 = -14.731216, constant1 = 1163.700316;
    double quadCoeff2 = -0.000516, linearCoeff2 = 0.219606, constant2 = 21.178778;
    double stripReading, nitrateConc;
    File sdRoot;
    String dir;
    String fileName;

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully!!");
        } else {
            Log.i(TAG, "OpenCV not loaded");
        }
    }

    private BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                populate();
            } else {
                super.onManagerConnected(status);
            }
        }
    };

    @OnClick(R.id.btnSelectImage)
    void selectImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, 1);
    }

    @OnClick(R.id.btnClickImage)
    void clickImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        sdRoot = Environment.getExternalStorageDirectory();
        dir = "/DCIM/Farmss/";
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        fileName = dateFormat.format(date) + ".jpg";
        final File mkdir = new File(sdRoot, dir);
        if (!mkdir.exists()) {
            mkdir.mkdirs();
        }
        final File file = new File(sdRoot, dir + fileName);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        Uri contentUri = Uri.fromFile(file);
        Intent mediaIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaIntent.setData(contentUri);
        NitrateTestActivity.this.sendBroadcast(mediaIntent);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @OnClick(R.id.btnAnotherTest)
    void takeAnotherTest() {
        if (txtGvalue.getText().toString().equals(getResources().getString(R.string.strip_g_value))){
            Snackbar.show(this, getString(R.string.test_alert));
            return;
        }else {
            onBackPressed();
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_nitrate);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(NitrateTestActivity.this, getResources().getString(R.string.nitrate_test)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnClickImage.setBackgroundResource(R.drawable.ripple_rounded);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSelectImage.setBackgroundResource(R.drawable.ripple_rounded);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnAnotherTest.setBackgroundResource(R.drawable.ripple_rounded);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        mat = new Mat(bitmap.getWidth(), bitmap.getHeight(), CvType.CV_8UC4);
                        Utils.bitmapToMat(bitmap, mat);
                        Bitmap bitmap1 = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                        Utils.matToBitmap(mat, bitmap1);
                        imgStripPicture.setImageBitmap(bitmap1);
                        imageInfo();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    File file = new File(sdRoot, dir + fileName);
                    Bitmap photo = decodeSampledBitmapFromFile(file.getAbsolutePath(), 3264, 3264);
                    mat = new Mat(photo.getWidth(), photo.getHeight(), CvType.CV_8UC4);
                    Utils.bitmapToMat(photo, mat);
                    Bitmap picture = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mat, picture);
                    imgStripPicture.setImageBitmap(picture);
                    //imgPicture.setRotation(90);
                    imageInfo();
                }
                break;
        }
    }

    private void imageInfo() {

        double lR1 = 0, lG1 = 0, lB1 = 0;
        double[] lVal1 = {};
        for (int i = 1725; i <= 1775; i++) {
            for (int j = 775; j <= 825; j++) {
                lVal1 = mat.get(i, j);
                lR1 += lVal1[0];
                lG1 += lVal1[1];
                lB1 += lVal1[2];
            }
        }
        int lR1_avg = (int) (lR1 / 2601);
        int lG1_avg = (int) (lG1 / 2601);
        int lB1_avg = (int) (lB1 / 2601);

        deltaR = lR1_avg - r_ref;
        deltaG = lG1_avg - g_ref;
        deltaB = lB1_avg - b_ref;

        double r = 0, g = 0, b = 0;
        double[] val = {};
        for (int i = 2110; i <= 2230; i++) {
            for (int j = 1135; j <= 1255; j++) {
                val = mat.get(i, j);
                r += val[0];
                g += val[1];
                b += val[2];
            }
        }
        int r_avg = (int) (r / 14641);
        int g_avg = (int) (g / 14641);
        int b_avg = (int) (b / 14641);

        int r_n_final = r_avg - deltaR;
        int g_n_final = g_avg - deltaG;
        int b_n_final = b_avg - deltaB;

        txtGvalue.setText(getResources().getString(R.string.strip_g_value) + g_avg);
        txtCorrectedGValue.setText(getResources().getString(R.string.corrected_g_value) + g_n_final);

        stripReading = (quadCoeff1 * g_n_final * g_n_final) + (linearCoeff1 * g_n_final) + constant1;
        txtStripReading.setText(getResources().getString(R.string.strip_reading) + stripReading);

        nitrateConc = (quadCoeff2 * stripReading * stripReading) + (linearCoeff2 * stripReading) + constant2;
        txtNitrateConcentration.setText(getResources().getString(R.string.nitrate_concentration) + nitrateConc);
        Double d = Math.round(nitrateConc * 100) / 100.0d;
        preferences.edit().putString("nitrate", String.valueOf(d)).commit();
    }

    private Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, baseLoaderCallback);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
        if (txtGvalue.getText().toString().equals(getResources().getString(R.string.strip_g_value))){
            Snackbar.show(this, getString(R.string.test_alert));
            return;
        } else {
            super.onBackPressed();
        }
    }
}

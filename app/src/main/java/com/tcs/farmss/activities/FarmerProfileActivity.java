package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 1/20/2017.
 */
public class FarmerProfileActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnTakeTest)
    ButtonPlus btnTakeTest;
    @Bind(R.id.txtFarmerName)
    TextView txtFarmerName;
    @Bind(R.id.txtMobileNumber)
    TextView txtMobileNumber;
    @Bind(R.id.txtAddress)
    TextView txtAddress;
    SharedPreferences preferences;

    @OnClick(R.id.btnTakeTest)
    void takeTest() {
        new MaterialDialog.Builder(FarmerProfileActivity.this)
                .title(R.string.plot_size).customView(R.layout.enter_plot_size_custom_view, true).inputType(InputType.TYPE_CLASS_NUMBER)
                .positiveText(R.string.ok)
                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").contentGravity(GravityEnum.CENTER).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
                RadioButton radioAcre = (RadioButton) dialog.findViewById(R.id.radioAcre);
                RadioButton radioGunthe = (RadioButton) dialog.findViewById(R.id.radioGunthe);
                View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
                EditText plotSize = (EditText) dialog.getCustomView().findViewById(R.id.etPlotSize);
                if (plotSize.getText().toString().equals("")) {
                    Snackbar.show(FarmerProfileActivity.this, getString(R.string.plot_size_error));
                    return;
                } else {
                    Intent intent = new Intent(FarmerProfileActivity.this, SoilTestActivity.class);
                    if (radioAcre.isChecked()) {
                        intent.putExtra("plot size", plotSize.getText().toString() + " " + radioAcre.getText().toString());
                        startActivity(intent);
                    } else {
                        intent.putExtra("plot size", plotSize.getText().toString() + " " + radioGunthe.getText().toString());
                        startActivity(intent);
                    }
                }
            }
        }).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_profile);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;
        }
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.farmer_profile)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        final String farmerName, farmerMobile, farmerAddress;
        if (bundle != null) {
            farmerName = bundle.getString("name");
            farmerMobile = bundle.getString("mobile");
            farmerAddress = bundle.getString("village") + ", Tehsil: " + bundle.getString("tehsil") + ", District: " + bundle.getString("district")
                    + ", State: " + bundle.getString("state") + "- " + bundle.getString("pincode");

            txtFarmerName.setText(CustomTitle.getTitle(this, farmerName));
            txtMobileNumber.setText(CustomTitle.getPlainTitle(this, farmerMobile));
            txtAddress.setText(CustomTitle.getPlainTitle(this, farmerAddress));
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnTakeTest.setBackgroundResource(R.drawable.ripple);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

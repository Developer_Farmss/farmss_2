package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.button.ButtonPlus;
import com.tcs.farmss.util.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/19/2017.
 */
public class ChangeLanguageActivity extends LocalizationActivity implements View.OnClickListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.radioGrpLang)
    RadioGroup radioGrpLang;
    @Bind(R.id.radioEnglish)
    RadioButton radioEnglish;
    @Bind(R.id.radioHindi)
    RadioButton radioHindi;
    @Bind(R.id.radioMarathi)
    RadioButton radioMarathi;
    @Bind(R.id.btnChangeLanguage)
    ButtonPlus btnChangeLanguage;
    private SessionManager sessionManager;
    public Intent intent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.ChangeLanguage)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        sessionManager = new SessionManager(getApplicationContext());
        intent = new Intent(this, LoginActivity.class);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnChangeLanguage.setBackgroundResource(R.drawable.ripple);
        }

        btnChangeLanguage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (radioEnglish.isChecked()) {
            setLanguage("en");
            sessionManager.setLang("english");
            startActivity(intent);
        } else if (radioHindi.isChecked()) {
            setLanguage("hi");
            sessionManager.setLang("hindi");
            startActivity(intent);
        } else if (radioMarathi.isChecked()) {
            setLanguage("mr");
            sessionManager.setLang("marathi");
            startActivity(intent);
        }
    }
}
